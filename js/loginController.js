squamataSpendingClientApp.controller('loginController', function($scope, $location, Restangular, localStorageService) {
    $scope.user = {};
    $scope.login = function() {
        if (localStorageService.get('token') != null) {
             $location.path('/security/home');
        }
        var login = Restangular.allUrl('login', 'http://localhost:11001/spending/oauth');
        var response = login.customGET('token',
        {'password':$scope.user.password,
        'username':$scope.user.username,
        'grant_type':'password',
        'scope':'read%20write',
        'client_secret':'5d04164a-4207-4c20-987f-0eae77546da0',
        'client_id':'b5e1f727-68a5-4c0d-a71d-0d323ea25301'})
        .then(function(response) {
            localStorageService.set('token',response.access_token);
            $location.path('/security/home');
        },
        function(response) {
            $scope.message = response;
            console.log("Error with status code", response.status);
        });
    };
});