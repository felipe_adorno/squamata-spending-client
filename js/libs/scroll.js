$().ready(function() {
    $(".navbar-fixed-top").removeClass("top-nav-collapse");
    $(".navbar-default .navbar-nav>li>a").css({color: '#fff', 'font-weight': 'bold'});
    $(".navbar-brand").css({color: '#fff', 'font-weight': 'bold'});
});

$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
        $(".navbar-default .navbar-nav>li>a").css({color: '#777', 'font-weight': 'bold'});
        $(".navbar-brand").css({color: '#777', 'font-weight': 'bold'});
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
        $(".navbar-default .navbar-nav>li>a").css({color: '#fff', 'font-weight': 'bold'});
        $(".navbar-brand").css({color: '#fff', 'font-weight': 'bold'});
    }
});

$(function() {
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();

    });
});