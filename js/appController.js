var squamataSpendingClientApp = angular.module('squamataSpendingClientApp', ['restangular','ngRoute', 'LocalStorageModule']);

squamataSpendingClientApp.run(['$rootScope', '$location', 'localStorageService', 'Restangular', function ($rootScope, $location, localStorageService, Restangular) {
    $rootScope.$on('$routeChangeStart', function (event) {
        if (localStorageService.get('token') != null) {
            Restangular.setDefaultHeaders({Authorization: "Bearer "+localStorageService.get('token')+""})
            event.preventDefault();
        } else {
            $location.path('/');
        }
    });
}]);

squamataSpendingClientApp.config(function($routeProvider) {
    $routeProvider

    .when('/', {
         templateUrl : '/pages/home.html',
         controller  : 'homeController',
    })

    .when('/security/home', {
        templateUrl : '/security/home.html',
        controller  : 'homeSecurityController',
    })

    .otherwise({
        redirectTo: '/error'
    });
});